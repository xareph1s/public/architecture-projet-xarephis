# Architecture Projet Xarephis

Il y a beaucoup de façon d'organiser un projet informatique. Il y a en a tellement qu'on se perd assez facilement.

Du coup, je vais en proposer une autre que j'utilise sur mes projets persos en hobby.

Ce document est donc surtout là pour ceux qui auraient besoin de lire la structure de mes projets pour X ou Y raisons.

## Périmètre de ce document

On se limite uniquement à la structure visuelle d'un projet, on n'ira pas jusqu'à définir les règles de nommage d'un fichier.

De même, la structure qu'on parlera se trouvera dans le dossier principal où on stockera la majorité des fichiers codes. Par exemple, la structure expliquée dans ce document sera visible dans un "/src" d'un projet NodeJS ou bien dans un "/lib" d'un projet Flutter.

## Structure

### Arborescence

```
.
|- api
  |- methodeAA.X
  |- methodeAB.X
  |- methodeBA.X
  |- methodeBB.X
  |- ...
|- app
  |- controllers
    |- controllerA.X
    |- controllerB.X
    |- ...
  |- pages
    |- pageA.X
    |- pageB.X
    |- ...
  |- [widgets]
    |- widgetA.X
    |- widgetB.X
    |- ...
|- shared
  |- core
    |- constants.X
    |- functions.X
  |- errors
    |- exceptions.X
    |- failures.X
|- [logique métier A]
  |- entities
    |- entitiesAA.X
    |- entitiesAB.X
    |- ...
  |- repositories
    |- repositoriesA.X
    |- [repositoriesLocalDataSourceA.X]
    |- [repositoriesRemoteDataSourceA.X]
|- [logique métier B]
  |- entities
    |- entitiesAA.X
    |- entitiesAB.X
    |- ...
  |- repositories
    |- repositoriesB.X
    |- [repositoriesLocalDataSourceB.X]
    |- [repositoriesRemoteDataSourceB.X]
|- ...

```

### Explication

* api : Les méthodes qui seront utilisées dans l'application (Vues, etc.)
* app : La partie "visuelles" d'un projet si le projet a pour vocation d'avoir une interface graphique
* app/controller : Les controllers de qui lient les vues et les modèles
* app/pages : Une page d'interface graphique
* app/widgets : un "module" d'une page
* shared : Toutes les méthodes/fonctions qui sont partagées
* shared/core : Toutes les méthodes/constantes partagées dans tout le projet
* shared/errors : Les erreurs/échecs dans l'application
* [logique métier] : C'est le dossier où il y a les modèles/connexions métiers
* [logique métier]/entities : Tous les modèles sont stockées ici
* [logique métier]/repositories : Les connexions vers une base de données sont écrites ici

### Licence

LICENCE PUBLIQUE DE L'UNION EUROPÉENNE v. 1.2 